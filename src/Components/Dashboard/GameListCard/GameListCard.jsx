import React, { useState, useEffect } from "react";
import { Card, Button, Modal } from 'antd';
import './gamelistcard.scss';
import mask_group_green from '../../../assets/img/mask_group_green.svg'
import mask_group_black from '../../../assets/img/mask_group_black.svg'


function GameListCard({ place, money, gel, showRegistration }) {

    return (
        <div>
            <Card className='registration-card'>
                <div className="card-header">
                    {place}
                    <div className="line-dot">
                        <div className='header-line'></div>
                        <div className="header-dot"></div>
                    </div>
                </div>
                <div className="card-money">
                    <h2 className="card-money-h2">{money}<span> GEL</span></h2>
                    <span className='max-mem'>მაქს. მოგება</span>
                </div>
                <div className='user-avatas'>
                    <img src={mask_group_green} alt="" />
                    <img src={mask_group_green} alt="" />
                    <img src={mask_group_black} alt="" />
                    <img src={mask_group_black} alt="" />
                    <img src={mask_group_black} alt="" />
                </div>
                <div className='card-buttons'>
                    <div style={{ display: 'flex', justifyContent: 'center', padding: "0px 0px 15px 0px" }}>
                        <Button className="gel-btn" shape="round" size='large'>{gel} ლარი</Button>
                    </div>
                    <Button className='registration-btn' onClick={() => showRegistration()}>რეგისტრაცია</Button>
                </div>
            </Card>
        </div>
    )

}

export default GameListCard
