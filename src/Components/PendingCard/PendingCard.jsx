import React, { useState, useEffect } from 'react';
import './pendingcard.scss';
import { Button } from 'antd';
import cb from '../../assets/img/cb.svg';
import cg from '../../assets/img/cg.svg';

function PendingCard() {

    return (
        <div>
            <div className="pre-header">
                <div className="pre-hed-left">
                    <span>5 კაციანი</span>
                    <div className="line-dot">
                        <div className="line"></div>
                        <div className="dot"></div>
                    </div>
                </div>
                <div className="pre-hed-righ">
                    <Button>5 ლარი</Button>
                    <h1>5000 <span>GEL</span></h1>
                </div>
            </div>
            <div className="pre-body">
                <h2>გთხოვთ დაელოდოთ მოწინააღდმეგეს</h2>
                <div className="user-list">
                    <img src={cg} alt="user" />
                    <img src={cg} alt="user" />
                    <img src={cg} alt="user" />
                    <img src={cb} alt="user" />
                    <img src={cb} alt="user" />
                </div>
                <span className="loading">სავარაუდო მოლოდინის დრო: 30 წამი</span>
                <div className="progress">
                    <div className="bar"></div>
                </div>
            </div>

        </div>
    )

}

export default PendingCard
