import React, { useState, useEffect } from "react";
import { Card, Button } from 'antd';
import './leftsidecard.scss';
import logo from '../../../assets/img/logo.svg';
import maskGroupCoin from '../../../assets/img/mask_group_coin.svg';
import userActive from '../../../assets/img/user-active.svg'
import bowl from '../../../assets/img/bowl.svg';

function LeftSideCard() {
    const [cards, setCard] = useState([
        {
            id: '1',
            name: 'Alberto',
            status: 0
        },
        {
            id: '2',
            name: 'Alberto',
            status: 0
        }, {
            id: '3',
            name: 'Alberto',
            status: 0
        },
        {
            id: '4',
            name: 'Davitich',
            status: 1
        },
        {
            id: '5',
            name: 'Alberto',
            status: 0
        }
    ])

    return (
        <div className="left-side">
            <Card className="left-side-card">
                <img src={logo} alt="logo" />
                <h1>5000<span> GEL</span></h1>
                <span>საპრიზო ფონდი</span>
            </Card>
            <h2 className="title"> <img src={bowl} alt="" /> ლიდერბორდი</h2>
            {
                cards.map((e, k) => {
                    return (
                        <div className={`coin-card ${e.status === 1 ? 'active' : ''}`} key={k}>
                            <div className="coin-card-body">
                                <div className="side-group">
                                    <div className={`counter ${e.status === 1 ? 'yellow' : ''}`}>{e.id}</div>
                                    <div className="user-info">
                                        <span className="user-info-span"><img src={e.status === 1 ? userActive : maskGroupCoin} alt="" /> {e.name}</span>
                                        <h2 className={e.status === 1 ? 'yellow-text' : ''}>322145</h2>
                                    </div>
                                </div>
                                <div className="right-side">
                                    <span id='rs-1'>3264</span>
                                    <span id='rs-2'>COIN</span>
                                </div>
                            </div>
                        </div>
                    )
                })
            }
        </div>
    )

}

export default LeftSideCard
