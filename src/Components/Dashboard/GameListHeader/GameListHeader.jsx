import React, { useState, useEffect } from "react";
import './gamelistheader.scss';
import logo from '../../../assets/img/logo.svg'

function GameListHeader() {

    return (
        <div className="game-list-geader">
            <img src={logo} alt="logo" />

            <div className="jeckpot-counter">
                <div className="jeckpot-counter-body">
                    <div className='counter-item'>8</div>
                    <div className='counter-item'>2</div>
                    <div className='counter-item'>5</div>
                    <div className='counter-item'>9</div>
                    <div className='counter-item'>1</div>
                    <div className='counter-dot'></div>
                    <div className='counter-sub-item'>9</div>
                    <div className='counter-sub-item'>2</div>
                </div>
                <div className="jeckpot-counter-footer">
                    <span>LEADER JACKPOT</span>
                </div>
            </div>

            <div className="jeckpot-counter">
                <div className="jeckpot-counter-body">
                    <div className='counter-item'>8</div>
                    <div className='counter-item'>2</div>
                    <div className='counter-item'>5</div>
                    <div className='counter-item'>9</div>
                    <div className='counter-item'>1</div>
                    <div className='counter-dot'></div>
                    <div className='counter-sub-item'>9</div>
                    <div className='counter-sub-item'>2</div>
                </div>
                <div className="jeckpot-counter-footer">
                    <span>LEADER JACKPOT</span>
                </div>
            </div>
        </div>
    )

}

export default GameListHeader
