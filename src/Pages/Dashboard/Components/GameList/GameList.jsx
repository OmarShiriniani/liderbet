import React, { useState, useEffect } from "react";
import { Tabs, Buttonm, Modal } from 'antd';
import GameListCard from '../../../../Components/Dashboard/GameListCard';
import Table from '../Table'
import mask_group_green from '../../../../assets/img/mask_group_green.svg';
import './gamelist.scss';
import PreStartGame from '../../../../Components/PreStartGame';
import PendingCard from '../../../../Components/PendingCard'

function GameList() {
    const [cardList, setCardList] = useState([
        {
            place: '5 კაციანი',
            money: '5000',
            users: '5',
            gel: '5',
        },
        {
            place: '5 კაციანი',
            money: '5000',
            users: '5',
            gel: '5',
        },
        {
            place: '5 კაციანი',
            money: '5000',
            users: '5',
            gel: '5',
        },
        {
            place: '5 კაციანი',
            money: '5000',
            users: '5',
            gel: '5',
        },
        {
            place: '5 კაციანი',
            money: '5000',
            users: '5',
            gel: '5',
        },
        {
            place: '5 კაციანი',
            money: '5000',
            users: '5',
            gel: '5',
        },
        {
            place: '5 კაციანი',
            money: '5000',
            users: '5',
            gel: '5',
        },
        {
            place: '5 კაციანი',
            money: '5000',
            users: '5',
            gel: '5',
        }
    ])

    const { TabPane } = Tabs;

    const showRegistration = () => {
        Modal.info({
            className: "preModal",
            okText: 'დაწყება',
            content: <PreStartGame />,
            onOk() {
                showPendingCard()
            },
        });
    }

    const showPendingCard = () => {
        Modal.info({
            className: "preModal",
            okText: 'დახურვა',
            content: <PendingCard />
        });
    }

    return (
        <div>
            <Tabs className="tab1" tabBarExtraContent={<div> <img src={mask_group_green} alt="user" /> Online Players: 4364</div>}>
                <TabPane tab="ALL GAMES" key="1">
                    <div className="card-list">

                        {
                            cardList.map(e => {
                                return (
                                    <GameListCard
                                        showRegistration={() => showRegistration()}
                                        place={e.place}
                                        money={e.money}
                                        users={e.users}
                                        gel={e.gel}
                                    />
                                )
                            })
                        }
                    </div>
                    <div className="table">
                        <Tabs defaultActiveKey="1">
                            <TabPane tab="ტოპ მოგებები" key="1"><Table /></TabPane>
                            <TabPane tab="ლიდერბორდი" key="2">ლიდერბორდი</TabPane>
                            <TabPane tab="ისტორია" key="3">ისტორია</TabPane>
                            <TabPane tab="წესები" key="4">წესები</TabPane>
                        </Tabs>
                    </div>
                </TabPane>
                <TabPane tab="ONLY 3 PLAYER" key="2"> <h1>ONLY 3 PLAYER</h1></TabPane>
                <TabPane tab="ONLY 5 PLAYER" key="3"> <h1>ONLY 5 PLAYER</h1></TabPane>
            </Tabs>
        </div>
    )

}

export default GameList
