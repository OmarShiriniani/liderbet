import React, { Component } from 'react'
import { Row, Col } from 'antd';
import './dashboard.scss';
import GameList from './Components/GameList';
import GameListHeader from '../../Components/Dashboard/GameListHeader';
import RightSide from './Components/RightSide';
import RightSideHeader from '../../Components/RightSideHeader';

export class Dashboard extends Component {
    render() {
        return (
            <div className='dashboard-body'>
                <Row gutter={[8, 8]}>
                    <Col flex="1 1 200px">
                        <GameListHeader />
                        <GameList />
                    </Col>
                    <Col flex="0 1 380px">
                        <RightSideHeader />
                        <RightSide />
                    </Col>
                </Row>
            </div>
        )
    }
}

export default Dashboard
