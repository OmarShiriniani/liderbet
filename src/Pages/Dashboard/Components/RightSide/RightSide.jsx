import React, { Component } from 'react';
import './rightside.scss';
import { Card, Button, Modal } from 'antd';
import logo from '../../../../assets/img/logo.svg';
import spinerLogo from '../../../../assets/img/spiner-logo.svg';
import balanceLogo from '../../../../assets/img/balance-logo.svg';
import time from '../../../../assets/img/time.svg';
import maskGroupGreen from '../../../../assets/img/mask_group_green.svg';
import HistoryModal from '../../../../Components/HistoryModal';

export class RightSide extends Component {

    state = {
        modal1Visible: false,
        cards: [
            {
                anount: '5000'
            },
            {
                anount: '5000'
            },
            {
                anount: '5000'
            },
            {
                anount: '5000'
            },
        ]
    }

    SubCards = (e, k) => {
        return (
            <div key={k}>
                <Card className="sub-card">
                    <div className="sub-card-head">
                        <h2>{e.anount} <span>GEL</span></h2>
                        <h3><img src={time} alt="tiem" /> 23.04.19 20:33</h3>
                    </div>
                    <div className="sub-card-body">
                        <img src={maskGroupGreen} alt="" />
                        <span>ნოდარა, ელდარა, ემზარა</span>
                    </div>
                    <div className="sub-card-footer">
                        <span>1000 X 2</span>
                    </div>
                </Card>
            </div>
        )
    }


    render() {
        const { cards, modal1Visible } = this.state
        return (
            <div>
                <Modal
                    title={<span style={{color:'#fff'}}>ისტორია</span>}
                    className='history-modal'
                    centered
                    visible={modal1Visible}
                    onOk={() => this.setState({ modal1Visible: false })}
                    onCancel={() => this.setState({ modal1Visible: false })}
                    footer={null}
                    width={964}
                    height={690}
                    bodyStyle={{
                        background: "#35364a",
                    }}
                    maskStyle={{
                        background: 'none'
                    }}
                >
                    <HistoryModal />
                </Modal>
                <Card className='top-card'>
                    <div className="history-btn-area">
                        <Button className="history-btn" onClick={() => this.setState({ modal1Visible: true })}>ისტორია</Button>
                    </div>
                    <div className="top-card-body">
                        <div className="cent-logo-body">
                            <div className="cent-logo">
                                <img src={logo} alt="logo" />
                            </div>
                            <h2>Muscul Bones</h2>
                        </div>
                        <div className="top-card-footer">
                            <div className="balance">
                                <span><img src={balanceLogo} alt="balance" /> ბალანსი</span>
                                <span>995.65 GEL</span>
                            </div>
                            <div className="free-spin">
                                <span><img src={spinerLogo} alt="spiner" /> უფასო სპინები</span>
                                <span>18 GEL</span>
                            </div>
                        </div>
                    </div>
                </Card>

                <Card className="bottom-card">
                    <div className="top-spiners">
                        <span>ტოპ სპინერები</span>
                        <div className="widget">
                            <div className="widget-line"></div>
                            <div className="widget-dot"></div>
                        </div>
                    </div>
                    {
                        cards.map((e, k) => (
                            this.SubCards(e, k)
                        ))
                    }
                </Card>
            </div>
        )
    }
}

export default RightSide
