import React from 'react';
import 'antd/dist/antd.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Dashboard from './Pages/Dashboard'
import EndGame from './Pages/EndGame'

function App() {
  return (
    <Router>
      <div>
        <Route exact path="/" component={Dashboard} />
        <Route exact path="/end-game" component={EndGame} />
      </div>
    </Router>
  );
}

export default App;
