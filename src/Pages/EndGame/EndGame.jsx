import React, { Component } from 'react'
import { Row, Col } from 'antd';
import './endgame.scss';
import LeftSideCard from '../../Components/EndGame/LeftSideCard';
import RightSideCard from '../../Components/EndGame/RightSideCard'

export class EndGame extends Component {
    render() {
        return (
            <Row>
                <Col flex="450px">
                    <div className="leftside">
                        <LeftSideCard />
                    </div>
                </Col>
                <Col flex="auto">
                    <RightSideCard />
                </Col>
            </Row>
        )
    }
}

export default EndGame
