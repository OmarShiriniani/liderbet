import React, { Component } from 'react'
import { Table } from 'antd';
import mask_group_green from '../../../../assets/img/mask_group_green.svg'
import time from '../../../../assets/img/time.svg'
import './subtable.scss';

export class SubTable extends Component {
    state = {
        columns: [
            {
                title: '',
                render: () => <img src={mask_group_green} alt="user" />,
            },
            {
                title: 'ნომერი',
                dataIndex: 'number',
                key: 'number',
                render: (e) => {
                    return (<div className="cl-wh">{e}</div>)
                },
            },
            {
                title: 'დრო',
                dataIndex: 'time',
                key: 'time',
                render: (e) => {
                    return (<div className="cl-wh"> <img src={time} alt="user" /> {e}</div>)
                },
            },
            {
                title: 'კოეფიციენტი',
                dataIndex: 'coefficient',
                key: 'coefficient',
                render: (e) => {
                    return (<div className="coefficient">{e}</div>)
                },
            },
            {
                title: 'მომხმარებელი',
                key: 'user',
                dataIndex: 'user',
                render: (e) => {
                    return (<div className="cl-wh"> <img src={mask_group_green} alt="user" /> {e}</div>)
                },
            },
            {
                title: 'მოგებული თანხა',
                key: 'win_amount',
                dataIndex: 'win_amount',
                render: (e) => {
                    return (<div className="win_amount">{e}</div>)
                },
            },
        ],
        data: [
            {
                key: '1',
                number: '# 4412452',
                time: '12:33',
                coefficient: '2.37',
                user: 'Nodard...',
                win_amount: '33.35',
            },
            {
                key: '2',
                number: '# 4412452',
                time: '12:33',
                coefficient: '2.37',
                user: 'Nodard...',
                win_amount: '33.35',
            },
            {
                key: '3',
                number: '# 4412452',
                time: '12:33',
                coefficient: '2.37',
                user: 'Nodard...',
                win_amount: '33.35',
            },
            {
                key: '3',
                number: '# 4412452',
                time: '12:33',
                coefficient: '2.37',
                user: 'Nodard...',
                win_amount: '33.35',
            },
            {
                key: '3',
                number: '# 4412452',
                time: '12:33',
                coefficient: '2.37',
                user: 'Nodard...',
                win_amount: '33.35',
            },

        ]
    }
    render() {
        const { columns, data } = this.state;
        return (
            <Table columns={columns} dataSource={data} pagination={false} />
        )
    }
}

export default SubTable
