import React, { Component } from 'react';
import './historymodal.scss';
import { Table } from 'antd';
import maskGroupGreen from '../../assets/img/green_user-sm.svg'

export class HistoryModal extends Component {
    render() {
        const columns = [
            {
                title: 'თარიღი',
                dataIndex: '1',
            },
            {
                title: 'ღირებუ.ი',
                dataIndex: '2',
            },
            {
                title: 'რა Xი',
                dataIndex: '3',
            },
            {
                title: 'რაოდენობა',
                dataIndex: '4',
            },
            {
                title: 'მონაწილეებიი',
                dataIndex: '5',
                width: '240px',
                render: (e) => {
                    return (<div className="users-t"> <img src={maskGroupGreen} alt="user" /> {e}</div>)
                },
            },
            {
                title: 'ადგილი',
                dataIndex: '6',
            },
            {
                title: 'მოგებული',
                dataIndex: '7',
                render: (e) => {
                    return (<div style={{ color: '#FFFF00' }}>{e}</div>)
                },
            },
        ];

        const data = [
            {
                key: '1',
                1: '23 იანვ.',
                2: ' 2 ლარი',
                3: '100X',
                4: '5 კაციანი ',
                5: 'ნოდარა, ელდარა, ემზარა, ზაირა, მერაბა',
                6: '10',
                7: '5000 ლარი',
            },
            {
                key: '1',
                1: '23 იანვ.',
                2: ' 2 ლარი',
                3: '100X',
                4: '5 კაციანი ',
                5: 'ნოდარა, ელდარა, ემზარა, ზაირა, მერაბა',
                6: '10',
                7: '5000 ლარი',
            },
            {
                key: '1',
                1: '23 იანვ.',
                2: ' 2 ლარი',
                3: '100X',
                4: '5 კაციანი ',
                5: 'ნოდარა, ელდარა, ემზარა, ზაირა, მერაბა',
                6: '10',
                7: '5000 ლარი',
            },

        ];
        return (
            <Table className="history-table" columns={columns} pagination={false} dataSource={data} size="middle" />
        )
    }
}

export default HistoryModal
