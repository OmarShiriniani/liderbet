import React, { useState, useEffect } from 'react';
import './prestartgame.scss';
import { Button } from 'antd';
import game from '../../assets/img/lucky_xmas_poster_1280x720_zh.png';
import userIcon from '../../assets/img/mask_group_green.svg';

function PreStartGame() {

    return (
        <div>
            <div className="pre-header">
                <div className="pre-hed-left">
                    <span>5 კაციანი</span>
                    <div className="line-dot">
                        <div className="line"></div>
                        <div className="dot"></div>
                    </div>
                </div>
                <div className="pre-hed-righ">
                    <Button>5 ლარი</Button>
                    <h1>5000 <span>GEL</span></h1>
                </div>
            </div>
            <div className="pre-body">
                <div className="card-1">
                    <span className='x5'>5X</span>
                    <span className='coef'>კოეფიციენტი</span>
                </div>

                <div className="card-2">
                    <img src={game} alt="" />
                    <span className='coef'>JAMING JARS</span>
                </div>
            </div>
            <div className="user-list">
                <img src={userIcon} alt="user" />
                <img src={userIcon} alt="user" />
                <img src={userIcon} alt="user" />
                <img src={userIcon} alt="user" />
                <img src={userIcon} alt="user" />
            </div>
        </div>
    )

}

export default PreStartGame
