import React from 'react';
import './rightsideheader.scss';
import cashier from '../../assets/img/cashier.svg'
import setting from '../../assets/img/setting.svg'
import { Button } from 'antd';

function RightSideHeader() {

    return (
        <div className="rightsideheader">
            <Button className="cashier" size={'large'}> <img src={cashier} alt="" /> Cashier</Button>
            <div className="tools">
                <img className='tools-item' src={setting} alt="" />
                <img className='tools-item' src={setting} alt="" />
                <img className='tools-item' src={setting} alt="" />
                <img className='tools-item' src={setting} alt="" />
            </div>
        </div>
    )

}

export default RightSideHeader
