import React from "react";
import { Card, Button } from 'antd';
import './rightsidecard.scss';
import bowl from '../../../assets/img/bowl.svg';

function RightSideCard() {

    return (
        <div className="right-side">
            <Card className="r-side-card">
                <div className="c1">
                    <img src={bowl} alt="bowl" />
                    <h2 style={{ color: '#fff' }}>გილოცავთ!</h2>
                    <span>თქვენ დაიკავეთ პირველი ადგილი</span>
                    <div className="wined-am">
                        <div className='wined-gel'>5000 <span>GEL</span></div>
                        <span>მოგებული თანხა</span>
                    </div>
                </div>
                <div className="c2">
                    <Button className='close-btn'>დახურვა</Button>
                </div>
            </Card>
        </div>
    )

}

export default RightSideCard
